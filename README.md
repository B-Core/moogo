# Moogo

Modular Synthetizer in Go

## Installation

+ Install Golang > 1.5
+ Install [govendor](https://github.com/kardianos/govendor) `go get -u github.com/kardianos/govendor`
+ Get dependencies (govendor sync)

## Dependencies

In order for the project to work, you have to build the portaudio dependency for your operating system.
Normally there are already available packages in the `dependencies` directory.
Just run `bash build.sh` and it should build the dependencies for your OS.

> Windows

+ [portaudio](https://app.assembla.com/spaces/portaudio/wiki/Notes_about_building_PortAudio_with_MinGW)
+ [dx9mgw](http://liballeg.org/old.html) Only for Windows
+ [asiosdk2.3](https://www.steinberg.net/en/company/developers.html) Only for Windows

> Linux

+ http://portaudio.com/docs/v19-doxydocs/compile_linux.html