#!/bin/bash

# POSIX command lookup
CMDS="7z"
LINUX_DEPS="libasound-dev"

for i in $CMDS
do
  # command -v will return >0 when the $i is not found
	command -v $i >/dev/null && continue || { echo "$i command not found."; exit 1; }
done

if [ $OS != "Windows_NT" ]
then
  echo "fetching dependencies for Linux: ${LINUX_DEPS}"
  sudo apt-get install $LINUX_DEPS
fi

echo "All dependencies are here"
make